var whoami = (function () {

    var self = {

        config: {
            persons:   [
                {
                    firstname: 'Peter',
                    lastname:  'Meier',
                    haircolor: 'brown',
                    eyecolor:  'blue'
                },
                {
                    firstname: 'Lisa',
                    lastname:  'Hansemann',
                    haircolor: 'red',
                    eyecolor:  'blue'
                }
            ],
            questions: [
                'Do you have %s eyes?',
                'Do you have %s hairs?'
            ]
        },

        persons: [],

        init: function () {

            self.startSystem();
        },

        startSystem: function () {
            var sys = this;
            $.each(this.config.persons, function (k, p) {
                sys.persons.push(new Person(p.firstname, p.lastname, p.haircolor, p.eyecolor))
            });


        }
    };

    return {
        init: self.init
    };
})();

$(document).ready(function () {
    whoami.init();
});
