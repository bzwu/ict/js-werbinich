//-----------------
// Variables
//-----------------
var is_playing = false;
player = 0;

//-----------------
// Code
//-----------------

function jquerystart () {

    init();

    function init () {

        // Ein neues Objekt wird erstellt und es werden Eigenschaften zugewiesen
        function Person (name, gender, haircolor, eyecolor, age) {
            this.Name = name;
            this.Gender = gender;
            this.Haircolor = haircolor;
            this.Eyecolor = eyecolor;
            this.Age = age;
        }

        // Personen hinzufügen und Eigenschaften bearbeiten
        Peter = new Person('Peter', 'Er', 'braun', 'braun', 33);
        Fabienne = new Person('Fabienne', 'Sie', 'blond', 'blau', 24);
        Marc = new Person('Marc', 'Er', 'schwarz', 'grau', 47);

        choose(); // Die choose-funktion wird aufgerufen
        start_loop(); // der komplette Prozess wird gestartet
    }

    // Überprüfung, welcher Spieler gewählt wurde
    function choose () {
        switch (player) {
            case 1:
                player = 'Peter';
                break;
            case 2:
                player = 'Fabienne';
                break;
            case 3:
                player = 'Marc';
                break;
            default:
                player = null;
        }

        //Ausgabe der gewählten Person
        if (player != null) {
            $('#name').html('Du spielst nun mit ' + player + '.');
        } else {
            console.error('FAIL');
        }

        // Ausgabe der Eigenschaften
        if (player === 'Peter') {
            $('#info').html(Peter.Gender + ' ist ' + Peter.Age + ' Jahre alt, hat ' + Peter.Haircolor + 'e Haare und ' + Peter.Eyecolor + 'e Augen.');
        } else if (player === 'Fabienne') {
            $('#info').html(Fabienne.Gender + ' ist ' + Fabienne.Age + ' Jahre alt, hat ' + Fabienne.Haircolor + 'e Haare und ' + Fabienne.Eyecolor + 'e Augen.');
        } else if (player === 'Marc') {
            $('#info').html(Marc.Gender + ' ist ' + Marc.Age + ' Jahre alt, hat ' + Marc.Haircolor + 'e Haare und ' + Marc.Eyecolor + 'e Augen.');
        } else {
            $('#title').html('Bitte w&auml;hle einen Charakter!').addClass('false');
            setTimeout(function () { // Seite wird nach einem Warnhinweis und 2.5s reloadet
                location.reload();
            }, 2500);
        }
    }

    //Das Spiel wird initialisiert und gestartet
    function start_loop () {
        if (player === 'Peter' || player === 'Fabienne' || player === 'Marc') {
            is_playing = true;

            // Die Variable x wird mit einer zufälligen Zahl befüllt
            /*x = Math.random();
             x = Math.round(x * 10);*/
            x = Math.floor((Math.random() * 10) + 1);

            // Die Zufallszahl x wird auf eine zahl gerundet
            if (x >= 1 && x < 4) {
                x = Peter;
            } else if (x >= 4 && x < 7) {
                x = Fabienne;
            } else if (x >= 7 && x < 11) {
                x = Marc;
            }

            if (x.Haircolor == undefined) {
                reset();
                $('#title').html('OUUPS! Es ist ein Fehler in der Engine aufgetreten.').addClass('error');
                $('#name').html('Wir m&uuml;ssen das Spiel leider neustarten :(');
                setTimeout(function () {
                    location.reload();
                }, 3000);
            }
            game(); // die Hauptfunktion wird gestartet
        }
    }

    //Hauptcode des Games
    function game () {

        q1(); // ruft erste frage auf

        // Erste Frage (q1)
        function q1 () {

            $('#quest').html('Hat deine Person ' + x.Haircolor + 'e Haare?'); // Frage wird gestellt

            /*
             * Der Spieler muss angeben ob die Haarfarbe mit
             * seiner Person übereinstimmt.
             *
             * 'JA' Button
             */

            $('#quest1Y').click(function () {
                // Button ID's werden geändert, damit man nicht nochmal die selbe Frage beantworten kann.
                $('#quest1Y').attr('id', 'quest2Y');
                $('#quest1N').attr('id', 'quest2N');
                if (Peter.Haircolor === x.Haircolor || Fabienne.Haircolor === x.Haircolor || Marc.Haircolor === x.Haircolor) {
                    $('#title').html('Dann ist deine gew&auml;hlte Person ' + x.Name + '.').addClass('final'); // Das ergebnis wird ausgegeben
                    reset();
                    hideBtn();
                }
            });

            /*
             * 'NEIN' Button
             */

            $('#quest1N').click(function () {
                // Button ID's werden geändert, damit man nicht nochmal die selbe Frage beantworten kann.
                $('#quest1Y').attr('id', 'quest2Y');
                $('#quest1N').attr('id', 'quest2N');
                not = x.Name; // Die variable 'not' wird mit der Person, die es NICHT sein kann befüllt.
                $('#result').html('Schade, dann versuche ich es nochmals...'); // Ausgabe
                setTimeout(function () {
                    q2(); // Frage 2 wird nach 2.5s gestartet.
                }, 2500);
            });
        }

        function q2 () {
            // Die erste Frage wird resetted
            $('#result').html('&nbsp;');

            // Die zweite Frage wird per Zufallsprinzip gestellt.
            if (not === Peter.Name) {
                one = Fabienne;
                two = Marc;
            } else if (not === Fabienne.Name) {
                one = Peter;
                two = Marc;
            } else if (not === Marc.Name) {
                one = Peter;
                two = Fabienne;
            }

            /*x = Math.random();
             x = Math.round(x * 10);*/
            x = Math.floor((Math.random() * 10) + 1);

            if (x >= 1 && x < 5) {
                x = one;
            } else if (x >= 5 && x < 11) {
                x = two;
            } else {
                console.log("Error " + x);
            }

            $('#quest').html('Hat deine Person ' + x.Eyecolor + 'e Augen?'); // Frage wird gestellt

            /*
             * Der Spieler muss angeben ob die Haarfarbe mit
             * seiner Person übereinstimmt.
             *
             * 'JA' Button
             */

            $('#quest2Y').click(function () {
                // Button ID's werden geändert, damit man nicht nochmal die selbe Frage beantworten kann.
                $('#quest2Y').attr('id', 'quest3Y');
                $('#quest2N').attr('id', 'quest3N');
                if (one.Eyecolor === x.Eyecolor || two.Eyecolor === x.Eyecolor) {
                    $('#title').html('Dann ist deine gew&auml;hlte Person ' + x.Name + '.').addClass('final'); // Das ergebnis wird ausgegeben
                    reset();
                    hideBtn();
                }
            });

            /*
             * 'NEIN' Button
             */

            $('#quest2N').click(function () {
                // Button ID's werden geändert, damit man nicht nochmal die selbe Frage beantworten kann.
                $('#quest2Y').attr('id', 'quest3Y');
                $('#quest2N').attr('id', 'quest3N');
                not = x.Name; // Die variable 'not' wird mit der Person, die es NICHT sein kann befüllt.
                $('#result').html('Schade, dann versuche ich es nochmals...'); // Ausgabe
                setTimeout(function () {
                    q3(); // Frage 3 wird nach 2.5s gestartet.
                }, 2500);
            });

        }

        function q3 () {
            reset();
            if (one.Name === not) {
                x = two;
            } else if (two.Name === not) {
                x = one;
            }

            $('#title').html('Dann ist deine gew&auml;hlte Person ' + x.Name + '.').addClass('final'); // Das ergebnis wird ausgegeben
            hideBtn();
        }

    }
}