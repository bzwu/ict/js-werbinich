// MODAL
new function () {
    $('#choose').modal('toggle');
};

$('.peter').click(function () {
    player = 1;
    $('#choose').modal('toggle');
    jquerystart();
});

$('.fabienne').click(function () {
    player = 2;
    $('#choose').modal('toggle');
    jquerystart();
});

$('.marc').click(function () {
    player = 3;
    $('#choose').modal('toggle');
    jquerystart();
});

$('.closeChoose').click(function () {
    player = null;
    jquerystart();
});

// reset function
function reset () {
    $('#info').html('&nbsp;');
    $('#name').html('&nbsp;');
    $('#quest').html('&nbsp;');
    $('#result').html('&nbsp;');
}

// Buttons verstecken, wenn result angezeigt wird
function hideBtn () {
    if ($('#title').hasClass('final')) {
        $('input').hide();
        $('#info').hide();
        $('#name').hide();
        $('#quest').hide();
        $('#result').hide();
        $('.endgame').show();
    }
}